/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.uploads.nettigatujuhuploads.utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;
import javax.imageio.ImageIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Moh Machfudh
 */
@Service
public class ImageService {
    
    private final Logger logger = LoggerFactory.getLogger(ImageService.class);
    
    public static final String THUMBPREFIX = "thumb-";
    public static final String MEDIUMPREFIX = "med-";
    public static final String ORIGINALPREFIX = "ori-";
    public static final String BANNERPREFIX = "banner-";

    public static final String BRANDDIR = "brand";
    public static final String AGENTDIR = "agent";
    public static final String CATEGORYDIR = "category";
    public static final String CATEGORYICONDIR = "icon-category";
    public static final String PRODUCTDIR = "product";
    public static final String TOKODIR = "toko";
    public static final String PROFILE = "profile";
    public static final String KTPDIR = "ktp";
    public static final String BUKUTABUNGANDIR = "bukutab";
 
    @Value("${app.folder.upload}")
    public String uploadDir;
    
    public File moveFile(MultipartFile multipartFile, String tpe, String extension) {
        try {
            String folder = uploadDir + File.separator + tpe + File.separator;
            File folderImages = new File(folder);
            if (!folderImages.exists()) {
                logger.debug("Creating folder [{}]", folderImages.getAbsolutePath());
                Files.createDirectories(folderImages.toPath());
            }
            
            String random = UUID.randomUUID().toString().substring(0, 7);
            logger.info("random [{}]", random);
            logger.info("multipartFile [{}]", multipartFile.getSize());
            
            //
            
            String fileName = multipartFile.getOriginalFilename();
            int occurance = StringUtils.countOccurrencesOf(fileName, ".");
            if(occurance > 1) {
                for(int i = 0; i < occurance - 1; i++){
                    fileName = fileName.replaceFirst("\\.","-");
		
                }
            }
            
            
            fileName = fileName.replace(" ", "-");
            fileName = fileName.replace("_", "-");
            fileName = fileName.replaceAll("[^\\w\\-\\.]", "");
            String name = random + "-" + fileName;
            
            //return file
            File returnFile = new File(folder +  name);

            //save original
            File originalFile = new File(folder + ORIGINALPREFIX + name);
            logger.info("original File [{}]", originalFile.getPath());
            Files.copy(multipartFile.getInputStream(), originalFile.toPath());

            //save Medium
            BufferedImage mediumImage = createMedium(originalFile);
            String mediumFilename = folder + MEDIUMPREFIX + name;
            ImageIO.write(mediumImage, extension, new File(mediumFilename));
            
            //save thumbnail
            BufferedImage thumbImage = createThumbnail(originalFile);
            String thumbFilename = folder + THUMBPREFIX + name;
            ImageIO.write(thumbImage, extension, new File(thumbFilename));
            
            return returnFile;
        } catch (IOException ex) {
            logger.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    private Path load(String folder, String filename){
        return new File(uploadDir).toPath().resolve(folder+"/"+filename);
    }
    
    public BufferedImage createMedium(File file) throws IOException {
        Image image = (Image) ImageIO.read(file);
        int thumbWidth = 640;// Specify image width in px
        int thumbHeight = 640;// Specify image height in px

        int imageWidth = image.getWidth(null);// get image Width
        int imageHeight = image.getHeight(null);// get image Height

        double thumbRatio = (double) thumbWidth / (double) thumbHeight;
        double imageRatio = (double) imageWidth / (double) imageHeight;

        if (thumbRatio < imageRatio) {
            thumbHeight = (int) (thumbWidth / imageRatio);
        } else {
            thumbWidth = (int) (thumbHeight * imageRatio);
        }

        BufferedImage mediumImage = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_RGB);

        Graphics2D graphics = mediumImage.createGraphics();
        graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);
        graphics.dispose();

        return mediumImage;
    }

    public BufferedImage createThumbnail(File file) throws IOException {
        Image image = (Image) ImageIO.read(file);
        int thumbWidth = 100;// Specify image width in px
        int thumbHeight = 100;// Specify image height in px

        int imageWidth = image.getWidth(null);// get image Width
        int imageHeight = image.getHeight(null);// get image Height

        double thumbRatio = (double) thumbWidth / (double) thumbHeight;
        double imageRatio = (double) imageWidth / (double) imageHeight;

        if (thumbRatio < imageRatio) {
            thumbHeight = (int) (thumbWidth / imageRatio);
        } else {
            thumbWidth = (int) (thumbHeight * imageRatio);
        }

        BufferedImage thumbImage = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_RGB);

        Graphics2D graphics = thumbImage.createGraphics();
        graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);
        graphics.dispose();

        return thumbImage;
    }
    
}
