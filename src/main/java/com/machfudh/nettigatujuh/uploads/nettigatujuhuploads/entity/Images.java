/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.uploads.nettigatujuhuploads.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Moh Machfudh
 */
@Data
@Entity @Table(name="net_images")
public class Images {
    
     private static final long serialVersionUID = -7371610187321351709L;
    
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2") 
    private String id;
    
    @NotNull @NotEmpty
    private String milikid;
    
    private String thumbnail;
    private String medium;
    private String original;
    private String insertid;
    private String insertdate;
    
}
