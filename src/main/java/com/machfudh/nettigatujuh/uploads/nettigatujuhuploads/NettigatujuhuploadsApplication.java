package com.machfudh.nettigatujuh.uploads.nettigatujuhuploads;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NettigatujuhuploadsApplication {

	public static void main(String[] args) {
		SpringApplication.run(NettigatujuhuploadsApplication.class, args);
	}

}
