/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.uploads.nettigatujuhuploads.controller;

import ch.qos.logback.core.status.Status;
import com.machfudh.nettigatujuh.uploads.nettigatujuhuploads.dao.ImagesDao;
import com.machfudh.nettigatujuh.uploads.nettigatujuhuploads.entity.Images;
import com.machfudh.nettigatujuh.uploads.nettigatujuhuploads.utils.ImageService;
import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Moh Machfudh
 */
@Controller
public class ImagesController {

    private Logger logger = LoggerFactory.getLogger(ImagesController.class);
    private final List<String> FILE_EXTENSION = Arrays.asList("png", "jpg", "jpeg");

    @Autowired
    public ImagesDao imagesDao;

    @Autowired
    public ImageService imageService;
    
    private Date tglsnow = new Date();

    @Value("${app.base.url}")
    private String baseUrl;

    @PostMapping("api/images/catagory/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void imagesCatagory(@PathVariable(name = "id") String id, @RequestParam("file") MultipartFile profilePic) {
        Images images = new Images();
        
        if (profilePic != null) {
            logger.debug("FIle Recived : " + profilePic);
            if( profilePic.getSize() > 2097152 ){
                System.out.println("=============================== file terlalu besar");
            }
             String extention = tokenizer(profilePic.getOriginalFilename(), ".");   
             File file = imageService.moveFile(profilePic, ImageService.CATEGORYDIR, extention);
             images.setMilikid(id);
             images.setThumbnail(file.getName());
             images.setInsertdate(Long.toString(tglsnow.getTime()));
             imagesDao.save(images);
        }
    }
    
    @PostMapping("api/images/brand/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void imagesBrand(@PathVariable(name = "id") String id, @RequestParam("file") MultipartFile profilePic) {
        Images images = new Images();
        
        if (profilePic != null) {
            logger.debug("FIle Recived : " + profilePic);
            if( profilePic.getSize() > 2097152 ){
                System.out.println("=============================== file terlalu besar");
            }
             String extention = tokenizer(profilePic.getOriginalFilename(), ".");   
             File file = imageService.moveFile(profilePic, ImageService.BRANDDIR, extention);
             images.setMilikid(id);
             images.setThumbnail(file.getName());
             images.setInsertdate(Long.toString(tglsnow.getTime()));
             imagesDao.save(images);
             
        }
    }
    
    @PostMapping("api/images/product/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void imagesProduct(@PathVariable(name = "id") String id, @RequestParam("file") MultipartFile profilePic) {
        Images images = new Images();
        
        if (profilePic != null) {
            logger.debug("FIle Recived : " + profilePic);
            if( profilePic.getSize() > 2097152 ){
                System.out.println("=============================== file terlalu besar");
            }
             String extention = tokenizer(profilePic.getOriginalFilename(), ".");   
             File file = imageService.moveFile(profilePic, ImageService.PRODUCTDIR, extention);
             images.setMilikid(id);
             images.setThumbnail(file.getName());
             images.setInsertdate(Long.toString(tglsnow.getTime()));
             imagesDao.save(images);
             
        }
    }
    
    @PostMapping("api/images/agent/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void imagesAgent(@PathVariable(name = "id") String id, @RequestParam("file") MultipartFile profilePic) {
        Images images = new Images();
        
        if (profilePic != null) {
            logger.debug("FIle Recived : " + profilePic);
            if( profilePic.getSize() > 2097152 ){
                System.out.println("=============================== file terlalu besar");
            }
             String extention = tokenizer(profilePic.getOriginalFilename(), ".");   
             File file = imageService.moveFile(profilePic, ImageService.AGENTDIR, extention);
             images.setMilikid(id);
             images.setThumbnail(file.getName());
             images.setInsertdate(Long.toString(tglsnow.getTime()));
             imagesDao.save(images);
             
        }
    }
    
    @PostMapping("api/images/toko/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void imagesToko(@PathVariable(name = "id") String id, @RequestParam("file") MultipartFile profilePic) {
        Images images = new Images();
        
        if (profilePic != null) {
            logger.debug("FIle Recived : " + profilePic);
            if( profilePic.getSize() > 2097152 ){
                System.out.println("=============================== file terlalu besar");
            }
             String extention = tokenizer(profilePic.getOriginalFilename(), ".");   
             File file = imageService.moveFile(profilePic, ImageService.TOKODIR, extention);
             images.setMilikid(id);
             images.setThumbnail(file.getName());
             images.setInsertdate(Long.toString(tglsnow.getTime()));
             imagesDao.save(images);
             
        }
    }
    
    @PostMapping("api/images/ktp/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void imagesKtp(@PathVariable(name = "id") String id, @RequestParam("file") MultipartFile profilePic) {
        Images images = new Images();
        
        if (profilePic != null) {
            logger.debug("FIle Recived : " + profilePic);
            if( profilePic.getSize() > 2097152 ){
                System.out.println("=============================== file terlalu besar");
            }
             String extention = tokenizer(profilePic.getOriginalFilename(), ".");   
             File file = imageService.moveFile(profilePic, ImageService.KTPDIR, extention);
             images.setMilikid(id);
             images.setThumbnail(file.getName());
             images.setInsertdate(Long.toString(tglsnow.getTime()));
             imagesDao.save(images);
             
        }
    }
    
    private String tokenizer(String originalFilename, String token) {
        StringTokenizer tokenizer = new StringTokenizer(originalFilename, token);
        String result = "";
        while (tokenizer.hasMoreTokens()) {
            result = tokenizer.nextToken();
        }
        return result;
    }

}
